tic
[data, data_labels] = loadPedestrianDatabase('pedestrian_train.cdataset');
[test_data, test_labels] = loadPedestrianDatabase('pedestrian_test.cdataset');
I = data(1,:);
Image = reshape(I, [160, 96]);
r = size(Image);
imshow(uint8(Image))
hog_feature = hog_feature_vector(Image);

showHog(hog_feature, r) 

[row column] = size(data)

hog_feature_array = [];
for i=1:row
    
    I = data(i,:);
    Image = reshape(I, [160, 96]);

    hog_feature = hog_feature_vector(Image);
    hog_feature_array = [hog_feature_array; hog_feature];
end

%%
SVMModel_ped = SVMtraining(hog_feature_array, data_labels);
%%

hog_feature_array_test = [];
for i=1:row
    
    I = test_data(i,:);
    Image = reshape(I, [160, 96]);

    hog_feature = hog_feature_vector(Image);
    hog_feature_array_test = [hog_feature_array_test; hog_feature];
end
%%

classifications = [];

for i = 1:size(hog_feature_array_test, 1)
    classfication = SVMTesting(hog_feature_array_test(i,:), SVMModel_ped);
    classifications = [classifications; classfication];
end

%% Evaluation

comparison = (test_labels==classifications);


Accuracy = sum(comparison)/length(comparison)
toc