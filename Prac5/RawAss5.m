tic
[data, data_labels] = loadPedestrianDatabase('pedestrian_train.cdataset');
[test_data, test_labels] = loadPedestrianDatabase('pedestrian_test.cdataset');
I = data(1,:);
Image = reshape(I, [160, 96]);
r = size(Image);
imshow(uint8(Image))
hog_feature = hog_feature_vector(Image);

showHog(hog_feature, r) 

[row column] = size(data)



%%
SVMModel_ped_raw = SVMtraining(data, data_labels);


%%

classifications = [];

for i = 1:size(data, 1)
    classfication = SVMTesting(test_data(i,:), SVMModel_ped_raw);
    classifications = [classifications; classfication];
end

%% Evaluation

comparison = (test_labels==classifications);


Accuracy = sum(comparison)/length(comparison)

%% Testing

shadowman = SVMModel_ped_raw.xsup .* SVMModel_ped_raw.w

%%
shadowman_avg = mean(shadowman, 1) 
any
shadowman_avg = shadowman_avg + abs(min(shadowman_avg))

shadowman_avg = rescale(shadowman_avg, 0, 255);
Image = reshape(shadowman_avg, [160, 96]);
r = size(Image);
imshow(uint8(Image))