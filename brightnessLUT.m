function Lut = brightnessLUT(c)

%Task 2, Step 1
Lut = 1:256;

if Lut < -c
    Lut = 0
else if Lut > 255 - c
    Lut = 255
else
    Lut = Lut + c
end

Lut=uint8(Lut);
end