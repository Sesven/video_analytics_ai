function [label] = NNTesting(test_image,NNModel, K)

%NNTESTING Summary of this function goes here
%   Detailed explanation goes here

max_images = size(NNModel.neighbours);
max_images = max_images(1,1);

distance_array = [];
for image_iter = 1:max_images
    distance = EuclideanDistance(test_image, NNModel.neighbours(image_iter,:));

    % Appending to data
    distance_array = [distance_array; distance];
end

sorted_dist_array = sort(distance_array);

% Finding minimum value within array
min_dist = min(distance_array(:,1));

% labels = find(distance_array==sorted_dist_array(K,:))

labels = [];
for i=1:K
    nth_min_dist = find(distance_array==sorted_dist_array(i,1));
    value_label = NNModel.labels(nth_min_dist);
    labels = [labels; value_label];
end

% Applies the found index from training set
label = mode(labels);

end

% Iterate over all the other images, see what is closest and get it's label
% Classify it if its closest by that label

