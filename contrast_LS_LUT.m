function Lut = contrast_LS_LUT(c, m)

%Task 2, Step 1
Lut = 1:256;

if Lut < -c/m
    Lut = 0;
else if Lut > (255 - c)/m
    Lut = 255;
else
    Lut = m*Lut + c;
end

Lut=uint8(Lut);
end