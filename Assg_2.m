
% Task 1
[boat256, map1] = imread('boat256.jpg');
[dome256, map2] = imread('dome256.jpg');

subplot(2,3,1); imshow(boat256, map1);
subplot(2,3,2), imshow(dome256, map2);
subplot(2,3,3), histogram(boat256,'BinLimits',[0, 256], 'BinWidth',1);
subplot(2,3,4), histogram(dome256,'BinLimits',[0, 256], 'BinWidth',1);



H=histogram(boat256,'BinLimits',[0, 256], 'BinWidth',1);
h=H.Values;

test = myHistogram(boat256, 256, 0);

LUT = brightnessLUT(50);
subplot(2,3,5), plot(LUT)


ContrastLUT = contrast_LS_LUT(50, 3)
subplot(2,3,6), plot(ContrastLUT)