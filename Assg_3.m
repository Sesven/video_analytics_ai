

file_name = 'viptraffic.avi';
file_info = aviinfo(file_name)

videoObj = VideoReader(file_name);

vidFrames = read(videoObj);

size(vidFrames)
%info = rendererinfo(gca)
%montage(vidFrames)
%implay(vidFrames)

frame = vidFrames(:,:,:,1);
%imshow(frame)
size(frame)

%Task 2
Bkg = vidFrames(:,:, :,end);
BkgGray = rgb2gray(Bkg)
size(Bkg)
size(BkgGray)

BkgGray2 = Bkg(:,:,1)/3 + Bkg(:,:,2)/3 + Bkg(:,:,3)/3;

nrframes = size(vidFrames);
nrframes = nrframes(1,4);
Th = 50;

VidObj2 = VideoWriter('resultTraffic.avi');
open(VidObj2);
MAP=colormap(gray(256));

BkgGray = bckGenerator(vidFrames, 15);
Mask = ones(7,7);

for t = 1:nrframes
    currentFrame =  vidFrames(:,:,:,t);
    currentFrameGray = rgb2gray(currentFrame);
    currentFrameGray = double(currentFrameGray);
    BkgGray = double(BkgGray);
    Blobs = abs(currentFrameGray - BkgGray) > Th;
    frame = im2frame(uint8(Blobs)*255, MAP);
    writeVideo(VidObj2, frame);
    subplot(2,3,1), imshow(uint8(currentFrameGray)), title(['Frame: ',num2str(t)])
    subplot(2,3,2), imshow(uint8(BkgGray)), title('Background')
    subplot(2,3,3), imshow(Blobs), title('Blobs '), colormap(gray)

    Blobbing = imclose(Blobs, Mask);
    Blobbing = imclose(Blobbing, Mask);
    Blobbing = imclose(Blobbing, Mask);
    subplot(2,3,4), imshow(Blobbing);


    BlobsLabel = bwlabel(Blobbing, 4);
    NumVehicles = max(max(BlobsLabel));
    BBs = [];
    for b = 1: NumVehicles
        [ys xs] = find(BlobsLabel == b);
        xmax = max(xs);
        xmin = min(xs);
        ymin = min(ys);
        ymax = max(ys);
        BB = [xmin ymin xmax ymax];
        BBs = [BBs; BB];
    end
    subplot(2,3,5), imshow(BlobsLabel);
    subplot(2,3,6), imshow(currentFrame), title('Detections'), hold on
    for b = 1: NumVehicles
        rectangle('Position', [BBs(b,1) BBs(b,2) BBs(b,3)-BBs(b,1)+1 BBs(b,4)-BBs(b,2)+1])
    end
    hold off
    pause(0.2)
end

close(VidObj2);