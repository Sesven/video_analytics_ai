function dEuc=EuclideanDistance(sample1, sample2)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

FeatureSize = size(sample1);
FeatureSize = FeatureSize(1,2);

Inner = sample1 - sample2;
Inner = (Inner).^2;
Inner = sum(Inner);
Inner = sqrt(Inner);

dEuc = Inner;

end

