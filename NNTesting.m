function [label] = NNTesting(test_image,NNModel)

%NNTESTING Summary of this function goes here
%   Detailed explanation goes here

max_images = size(NNModel.neighbours);
max_images = max_images(1,1);

distance_array = [];
for image_iter = 1:max_images
    distance = EuclideanDistance(test_image, NNModel.neighbours(image_iter,:));

    % Appending to data
    distance_array = [distance_array; distance];
end

% Finding minimum value within array
min_dist = min(distance_array(:,1));

% Finds the index of the array with the found min value
min_dist_index = find(distance_array==min_dist);

% Applies the found index from training set
label = NNModel.labels(min_dist_index);




end

% Iterate over all the other images, see what is closest and get it's label
% Classify it if its closest by that label

