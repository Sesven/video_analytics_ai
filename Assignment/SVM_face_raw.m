%% Getting data

[train_images, train_labels] = loadFaceImages('face_train.cdataset');
[test_images, test_labels] = loadFaceImages('face_test.cdataset');
%%
%Image = imresize(Image, 5);
%r = size(Image);


SVMModel_face = SVMtraining(train_images, train_labels);
%%
% For visualization purposes, we display the first 100 images
figure
for i=1:25

    % As you can notice by the size of the matrix image, each digit image
    % has been transform into a long feature vector to be fed in a machine
    % learning algorithm.
    
    %To visualise or recompose the image again, we need to revert that
    %process in its 28x28 image format
    Im = reshape(train_images(i,:),27,18);
    subplot(5,5,i), imshow(uint8(Im))
    
end
%histeq()
%%
Im = reshape(train_images(1,:),27,18);
[eq_image, ~] = histeq(uint8(Im), 255);
figure
subplot(1,2,1), imshow(eq_image);
subplot(1,2,2), imshow(uint8(Im));
%%
ImEqArray = [];

for i=1:25;
    Im = reshape(train_images(i,:),27,18);
    [eq_image, ~] = histeq(uint8(Im), 255);
    subplot(5,5,i), imshow(uint8(eq_image))
end
%% Evaluation


shadowman = SVMModel_face.xsup .* SVMModel_face.w

%%
shadowman_avg = mean(shadowman, 1) 
shadowman_avg = shadowman_avg + abs(min(shadowman_avg))

shadowman_avg = rescale(shadowman_avg, 0, 255);
Image = reshape(shadowman_avg, [27, 18]);
r = size(Image);
imshow(uint8(Image))