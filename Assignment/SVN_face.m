%% Getting data

[train_images, train_labels] = loadFaceImages('face_train.cdataset');
[test_images, test_labels] = loadFaceImages('face_test.cdataset');
%% Single image show
I = images(1,:);
Image = reshape(I, [27, 18]);
r = size(Image);
figure, hold on
title('Wrong Classification');
imshow(uint8(Image));
hold off
%Image = imresize(Image, 5);
%r = size(Image);
hog_feature = hog_feature_vector(Image);

showHog(hog_feature, r) 
%%
ImEqArray = [];

for i=1:size(train_images,1);

    Im = reshape(train_images(i,:),27,18);
    [eq_image, ~] = histeq(uint8(Im), 255);
    ImEqArray = [ImEqArray; Im(:)' ];
end

%% Getting HOG features and training
[row column] = size(train_images)

hog_feature_array = [];
for i=1:row
    
    I = ImEqArray(i,:);
    Image = reshape(I, [27, 18]);
    % Potential resize for more gradients

    hog_feature = hog_feature_vector(Image);
    hog_feature_array = [hog_feature_array; hog_feature];
end

SVMModel_face = SVMtraining(hog_feature_array, train_labels);

%% Getting test HOG features
hog_feature_array_test = [];
for i=1:size(test_images, 1)
    
    Im = reshape(test_images(i,:),27,18);
    [eq_image, ~] = histeq(uint8(Im), 255);

    hog_feature = hog_feature_vector(eq_image);
    hog_feature_array_test = [hog_feature_array_test; hog_feature];
end
%% Testing




classifications = [];

for i = 1:size(hog_feature_array_test, 1)
    classfication = SVMTesting(hog_feature_array_test(i,:), SVMModel_face);
    classifications = [classifications; classfication];
end

%% Evaluation

comparison = (test_labels==classifications);


Accuracy = sum(comparison)/length(comparison)